package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.databinding.ActivityRegisterBinding
import fr.isen.antunes.restaurantapp.model.Register
import fr.isen.antunes.restaurantapp.service.RestaurantAPIService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.ID_CLIENT
import fr.isen.antunes.restaurantapp.utils.displayToast
import org.koin.android.ext.android.inject

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var awesomeValidation: AwesomeValidation

    private val apiService by inject<RestaurantAPIService>()
    private val preferencesService by inject<UserPreferencesService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        awesomeValidation = AwesomeValidation(ValidationStyle.BASIC)
        binding = ActivityRegisterBinding.inflate( layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    private fun setupView() {
        textValidation( R.id.inputLastNameRegister, R.string.formInvalideLastName)
        textValidation( R.id.inputFirstNameRegister, R.string.formInvalidFirstName)
        textValidation( R.id.inputAddresseRegister, R.string.formInvalidAdresse)
        emailValidation( R.id.inputEmailRegister)
        passwordValidation( R.id.inputPasswordRegister)

        binding.buttonRegisterRegister.setOnClickListener {
            if ( awesomeValidation.validate()) {
                val register = Register(
                        binding.inputLastNameRegister.text.toString(),
                        binding.inputFirstNameRegister.text.toString(),
                        binding.inputAddresseRegister.text.toString(),
                        binding.inputEmailRegister.text.toString(),
                        binding.inputPasswordRegister.text.toString())

                Log.i( TAG, "register -> $register")
                displayToast( this, R.string.validation)
                register( register)
            } else {
                displayToast( this, R.string.error)
            }
        }

        binding.buttonLoginRegister.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun textValidation( id: Int, idMessage: Int) {
        awesomeValidation.addValidation(
                this,
                id,
                RegexTemplate.NOT_EMPTY,
                idMessage)
    }

    private fun emailValidation( id: Int) {
        awesomeValidation.addValidation(
                this,
                id,
                Patterns.EMAIL_ADDRESS,
                R.string.formInvalideEmail)
    }

    private fun passwordValidation(id: Int) {
        awesomeValidation.addValidation(
                this,
                id,
                PASSWORD_REGEX,
                R.string.formInvalidPassword)
    }

    private fun register( user: Register) {
        apiService.getRegister( apiService.getRegisterPostData( user)).observe( this, {
            it?.let {
                Log.i(CategoryActivity.TAG, "api resutl -> $it")
                preferencesService.writePreferences( ID_CLIENT, it.id.toString())
                startActivityLogin()
            } ?: run {
                Log.i(CategoryActivity.TAG, "no api result")
            }
        })
    }

    private fun startActivityLogin() {
        val intent = Intent( this, LoginActivity::class.java)
        this.startActivity(intent)
    }

    companion object {
        const val TAG = "RegisterActivity"
        const val PASSWORD_REGEX = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}"
    }
}