package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataRegisterResponse(
        @SerializedName("data")val data: RegisterResponse
) : Serializable

data class RegisterResponse(
        @SerializedName("id")val id: Int,
        @SerializedName("code")val code: String,
        @SerializedName("id_shop")val id_shop: Int
) : Serializable