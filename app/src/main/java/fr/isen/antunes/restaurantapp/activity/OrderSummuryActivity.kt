package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import fr.isen.antunes.restaurantapp.databinding.ActivityOrderSummuryBinding
import fr.isen.antunes.restaurantapp.enumeration.CommandStateEnum
import fr.isen.antunes.restaurantapp.utils.STATE_COMMAND

class OrderSummuryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOrderSummuryBinding
    private lateinit var commandState: CommandStateEnum

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderSummuryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    private fun setupView() {
        commandState =  intent.getSerializableExtra( STATE_COMMAND) as CommandStateEnum
        Log.i( TAG, "commandeState -> $commandState")
        binding.orderSummuryText.text = commandState.state

        binding.orderSummuryBackButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        const val TAG = "OrderSummuryActivity"
    }
}