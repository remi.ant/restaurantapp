package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Data(
    @SerializedName( "name_fr") val name: String,
    @SerializedName("items") val items: MutableList<Item>
) : Serializable
