package fr.isen.antunes.restaurantapp.modules

import fr.isen.antunes.restaurantapp.service.BasketService
import fr.isen.antunes.restaurantapp.service.RestaurantAPIService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModules = module {
    single{ RestaurantAPIService( androidContext(), UserPreferencesService( androidContext())) }
    single { UserPreferencesService( androidContext()) }
    single { BasketService( androidContext()) }
}