package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import android.util.Log
import android.view.Menu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.BASKET_COUNT
import org.koin.android.ext.android.inject

open class BaseActivity : AppCompatActivity() {

    private val preferencesService by inject<UserPreferencesService>()

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_toolbar, menu)
        val menuView = menu?.findItem(R.id.basket)?.actionView
        val countText = menuView?.findViewById(R.id.notificationText) as? TextView
        val articles = preferencesService.readPreferences( BASKET_COUNT)?.toInt() ?:run { null }

        countText?.isVisible = articles?.let { true }?:run { false }
        countText?.text = articles.toString()

        menuView?.setOnClickListener {
            val intent = Intent(this, BasketActivity::class.java)
            startActivity(intent)
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    companion object {
        const val TAG = "BaseActivity"
    }
}