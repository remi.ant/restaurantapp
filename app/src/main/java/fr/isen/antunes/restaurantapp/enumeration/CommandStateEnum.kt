package fr.isen.antunes.restaurantapp.enumeration

enum class CommandStateEnum( val state: String) {
    SUCCESSFUL("successful"),
    ERROR( "error")
}