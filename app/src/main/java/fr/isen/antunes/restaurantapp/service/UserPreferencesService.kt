package fr.isen.antunes.restaurantapp.service

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

interface UserPreferencesInterface {
    fun writePreferences( key:String, string: String)
    fun readPreferences( key: String): String?
    fun removeOnePreferences( key: String)
}

class UserPreferencesService(
        val context: Context
) : UserPreferencesInterface {

    override fun writePreferences( key:String, string: String) {
        context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE).edit().putString( key, string).apply()
    }

    override fun readPreferences( key: String): String? {
        return context.getSharedPreferences( APP_PREFERENCES, MODE_PRIVATE).getString( key, null)
    }

    override fun removeOnePreferences( key: String) {
        context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE).edit().remove( key).apply()
    }

    companion object {
        const val APP_PREFERENCES = "app_prefs"
    }
}