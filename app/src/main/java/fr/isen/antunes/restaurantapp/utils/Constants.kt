package fr.isen.antunes.restaurantapp.utils

const val CATEGORY = "category"
const val ITEM = "item"
const val ID_CLIENT = "id_client"
const val BASKET_COUNT = "basket_count"
const val STATE_COMMAND ="state_command"
const val QUERY_DISH_PREFERENCES = "query_preferences"
const val ZERO = 0
