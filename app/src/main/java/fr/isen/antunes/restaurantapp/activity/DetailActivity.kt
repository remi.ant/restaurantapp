package fr.isen.antunes.restaurantapp.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.adapter.DetailFragmentAdapter
import fr.isen.antunes.restaurantapp.databinding.ActivityDetailBinding
import fr.isen.antunes.restaurantapp.model.Item
import fr.isen.antunes.restaurantapp.model.OrderItem
import fr.isen.antunes.restaurantapp.service.BasketService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.BASKET_COUNT
import fr.isen.antunes.restaurantapp.utils.ITEM
import org.koin.android.ext.android.inject
import kotlin.math.max

class DetailActivity : BaseActivity() {

    private lateinit var binding: ActivityDetailBinding
    private val preferencesService by inject<UserPreferencesService>()
    private val basketService by inject<BasketService>()
    private var numberItem = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val item = intent.getSerializableExtra(ITEM) as? Item

        item?.let {
            setupView( it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    @SuppressLint("SetTextI18n")
    private fun setupView(item: Item) {
        binding.detailCardTitle.text = item.title
        binding.detailCardPrice.text = "${item.getPrice()} €"
        binding.detailCardDescription.text = item.getIngredients()
        binding.detailCardImage.adapter = DetailFragmentAdapter(this, item.images)

        updateQuantity( item)

        binding.detailCardButtonLess.setOnClickListener {
            numberItem = max( 1, numberItem - 1)
            updateQuantity( item)
            Log.i( TAG, "Decrease Item Quantity $numberItem")
        }

        binding.detailCardButtonMore.setOnClickListener {
            numberItem++
            updateQuantity( item)
            Log.i( TAG, "Increase Item Quantity $numberItem")
        }

        binding.detailCardOrderButton.setOnClickListener {
            basketService.saveBasketOrderItem( OrderItem( item, numberItem))
            alertUser( it)
            updateBasketQuantity()
            Log.i( TAG, "Add to basket $item")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateQuantity(item: Item) {
        val price = numberItem * item.prices.first().price.toFloat()
        binding.detailCardQuantity.text = numberItem.toString()
        binding.detailCardOrderButton.text = "order $price €"
    }

    private fun alertUser( view: View) {
        Snackbar.make( view, R.string.alertSnackBar, Snackbar.LENGTH_SHORT).show()
    }

    private fun updateBasketQuantity() {
        basketService.loadBasketOrder().let { orders ->
            orders.let {
                val count = orders.items.sumOf { it.quantity }
                preferencesService.writePreferences( BASKET_COUNT, count.toString())
                invalidateOptionsMenu()
            }
        }
    }

    companion object {
        const val TAG = "DetailActivity"
    }
}