package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.adapter.CartAdapter
import fr.isen.antunes.restaurantapp.databinding.ActivityBasketBinding
import fr.isen.antunes.restaurantapp.enumeration.CommandStateEnum
import fr.isen.antunes.restaurantapp.model.*
import fr.isen.antunes.restaurantapp.service.BasketService
import fr.isen.antunes.restaurantapp.service.RestaurantAPIService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.BASKET_COUNT
import fr.isen.antunes.restaurantapp.utils.ID_CLIENT
import fr.isen.antunes.restaurantapp.utils.STATE_COMMAND
import fr.isen.antunes.restaurantapp.utils.ZERO
import org.koin.android.ext.android.inject

class BasketActivity : BaseActivity() {

    private lateinit var binding: ActivityBasketBinding

    private val preferencesService by inject<UserPreferencesService>()
    private val apiService by inject<RestaurantAPIService>()
    private val basketService by inject<BasketService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBasketBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_toolbar, menu)
        val menuView = menu?.findItem(R.id.basket)?.actionView
        val countText = menuView?.findViewById(R.id.notificationText) as? TextView
        val articles = preferencesService.readPreferences( BASKET_COUNT)?.toInt() ?:run { null }

        countText?.isVisible = articles?.let { true }?:run { false }
        countText?.text = articles.toString()
        
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    private fun setupView() {
        basketService.loadBasketOrder().let { orders ->
            displayOrders(  orders.items)

            binding.shopBasketButton.setOnClickListener {
                isRegister( orders)
            }
        }
    }

    private fun isRegister(orders: Order) {
        val quantity = preferencesService.readPreferences( BASKET_COUNT)?.toInt() ?:run { -1 }

        if( quantity > -1) { val userID = preferencesService.readPreferences( ID_CLIENT)?.toInt() ?:run { -1 }
            if( userID > -1) {
                Log.i(TAG, "command item")
                doCommand(userID, orders)
            }else {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun displayOrders(orders: MutableList<OrderItem>) {
        binding.cartRecycleView.layoutManager = LinearLayoutManager(this)
        binding.cartRecycleView.adapter = CartAdapter(orders) { index ->
            basketService.deleteItemBasket( index)
            updateBasketQuantity()
            invalidateOptionsMenu()
        }
    }

    private fun updateBasketQuantity() {
        basketService.loadBasketOrder().let { orders ->
            orders.let { o ->
                Log.i( TAG, "order -> order ${o.items.size}")
                val count = o.items.sumOf { it.quantity }
                Log.i( TAG, "quantity of Basket -> $count")
                preferencesService.writePreferences( BASKET_COUNT, count.toString())
                invalidateOptionsMenu()
            }
        }
    }

    private fun doCommand( userID: Int, orders: Order) {

        apiService.getOrder( apiService.getOrderPostData( userID, orders)).observe(this, {
            Log.i(TAG, "command response $it")
            it?.let { response ->
                if (response.code == 200) {
                    commandDone()
                    startOrderSummaryActivity(CommandStateEnum.SUCCESSFUL)
                } else {
                    startOrderSummaryActivity(CommandStateEnum.ERROR)
                }
            } ?: run {
                Log.i(CategoryActivity.TAG, "no api result")
            }
        })
    }

    private fun commandDone() {
        preferencesService.writePreferences( BASKET_COUNT, ZERO.toString())
        basketService.deleteBasketFile()
        invalidateOptionsMenu()
    }

    private fun startOrderSummaryActivity(commandState: CommandStateEnum) {
        val intent = Intent(this, OrderSummuryActivity::class.java)
        intent.putExtra( STATE_COMMAND, commandState)
        startActivity(intent)
    }

    companion object {
        const val TAG = "BasketActivity"

    }
}