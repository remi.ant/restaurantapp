package fr.isen.antunes.restaurantapp

import android.app.Application
import fr.isen.antunes.restaurantapp.modules.applicationModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BocageApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            // Koin Android logger
            androidLogger()
            //inject Android context
            androidContext(this@BocageApplication)
            // use modules
            modules(applicationModules)
        }
    }
}