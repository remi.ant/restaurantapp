package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataJSON(
    @SerializedName("data") val data: List<Data>
) : Serializable
