package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Order(
        @SerializedName("data") val items: MutableList<OrderItem>
) : Serializable