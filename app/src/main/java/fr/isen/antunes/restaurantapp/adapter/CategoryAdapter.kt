package fr.isen.antunes.restaurantapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.databinding.ItemListBinding
import fr.isen.antunes.restaurantapp.model.Item

class CategoryAdaptor(
    private val categories: List<Item>,
    private val categoriesClickListener: (Item) -> Unit
) : RecyclerView.Adapter<CategoryAdaptor.CategoryHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder {
        val itemBinding =
            ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryHolder(itemBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        val item: Item = categories[position]

        holder.cardTitle.text = item.title
        holder.cardPrice.text = "${item.prices[0].price}€"

        if (item.images.first().isNotEmpty()) {
            Picasso.get()
                .load(categories[position].images[0])
                    .placeholder(R.drawable.ic_bocage)
                .into(holder.cardImage)
        } else {
            holder.cardImage.setImageResource(R.drawable.ic_bocage)
        }

        holder.layout.setOnClickListener {
            categoriesClickListener.invoke(item)
        }
    }

    override fun getItemCount(): Int = categories.size

    class CategoryHolder(binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        val cardTitle: TextView = binding.itemCardTitle
        val cardPrice: TextView = binding.itemCardPrice
        val cardImage: ImageView = binding.itemCardImage
        val layout = binding.root

    }
}