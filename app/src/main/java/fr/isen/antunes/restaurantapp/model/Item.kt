package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Item(
    @SerializedName( "name_fr") val title: String,
    @SerializedName("ingredients") val ingredients: List<Ingredient>,
    @SerializedName( "images") val images: List<String>,
    @SerializedName( "prices") val prices: List<Price>
) : Serializable {
    fun getIngredients(): String = ingredients.map { it.name }.joinToString(", ")
    fun getPrice() = prices[0].price.toDouble()
}