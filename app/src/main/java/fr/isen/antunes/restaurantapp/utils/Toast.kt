package fr.isen.antunes.restaurantapp.utils

import android.content.Context
import android.widget.Toast

fun displayToast( context: Context, resId: Int) {
    Toast.makeText( context, resId, Toast.LENGTH_SHORT).show()
}