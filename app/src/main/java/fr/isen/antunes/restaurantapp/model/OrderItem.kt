package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrderItem(
        @SerializedName("item")val item: Item,
        @SerializedName("quantity") var quantity: Int
) : Serializable