package fr.isen.antunes.restaurantapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.databinding.FragmentDetailPictureBinding

class DetailFragment: Fragment() {

    private lateinit var binding: FragmentDetailPictureBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailPictureBinding.inflate( inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.getString(ARG_OBJECT).let {

            if (!it.isNullOrEmpty()) {
                Picasso.get()
                        .load(it)
                        .into(binding.fragmentImage)
            } else {
                binding.fragmentImage.setImageResource( R.drawable.ic_bocage)
            }
        }
    }

    companion object {
        const val ARG_OBJECT = "object"
    }
}