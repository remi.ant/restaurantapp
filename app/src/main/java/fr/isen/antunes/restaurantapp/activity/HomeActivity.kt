package fr.isen.antunes.restaurantapp.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.databinding.ActivityHomeBinding
import fr.isen.antunes.restaurantapp.enumeration.CategoryEnum
import fr.isen.antunes.restaurantapp.utils.*
import fr.isen.antunes.restaurantapp.utils.displayToast

class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG,"destroy")
    }

    private fun setupView() {
        binding.buttonEntree.setOnClickListener {
            displayToast( this, R.string.menuStarter)
            startCategoryActivity( this, CategoryEnum.ENTREE)
        }

        binding.buttonPlat.setOnClickListener {
            displayToast( this, R.string.menuDish)
            startCategoryActivity( this, CategoryEnum.PLAT)
        }

        binding.buttonDesert.setOnClickListener {
            displayToast( this, R.string.menuDesert)
            startCategoryActivity( this, CategoryEnum.DESERT)
        }
    }

    private fun startCategoryActivity(context: Context, categoryType: CategoryEnum) {
        val intent = Intent(context, CategoryActivity::class.java)
        intent.putExtra(CATEGORY, categoryType)
        context.startActivity(intent)
    }

    companion object {
        const val TAG = "HomeActivity"
    }
}