package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import fr.isen.antunes.restaurantapp.databinding.ActivityLoginBinding
import fr.isen.antunes.restaurantapp.model.Register
import fr.isen.antunes.restaurantapp.service.RestaurantAPIService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.ID_CLIENT
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private val apiService by inject<RestaurantAPIService>()
    private val preferencesService by inject<UserPreferencesService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    private fun setupView() {

        binding.buttonLoginLogin.setOnClickListener {
            val user = Register( "", "", "",
                binding.inputEmailLoginForm.text.toString(),
                binding.inputPasswordLoginForm.text.toString())

            login( user)

        }

        binding.buttonRegisterLogin.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun login( user: Register) {
        apiService.getLogin( apiService.getLoginPostData( user)).observe( this, {
            it?.let {
                Log.i( TAG, "api resutl -> ${it.id}")
                preferencesService.writePreferences( ID_CLIENT, it.id.toString())
                startActivityBasket()
            }
        })
    }

    private fun startActivityBasket() {
        val intent = Intent( this, BasketActivity::class.java)
        this.startActivity(intent)
    }

    companion object {
        const val TAG = "LoginActivity"
    }
}