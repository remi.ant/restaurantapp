package fr.isen.antunes.restaurantapp.service

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import fr.isen.antunes.restaurantapp.activity.DetailActivity
import fr.isen.antunes.restaurantapp.model.Item
import fr.isen.antunes.restaurantapp.model.Order
import fr.isen.antunes.restaurantapp.model.OrderItem
import java.io.File

interface BasketInterface {
    fun saveBasketOrderItem( orderItem: OrderItem)
    fun loadBasketOrder(): Order
    fun deleteItemBasket( index: Int)
    fun deleteBasketFile()
}

class BasketService (val context: Context ) : BasketInterface {

    override fun loadBasketOrder(): Order {
        val file = File( context.cacheDir.absolutePath + FILE_NAME)
        lateinit var orders: Order

        if ( file.exists()) {
            orders = Gson().fromJson(file.readText(), Order::class.java) as Order
        }
        return orders
    }

    override fun saveBasketOrderItem( orderItem: OrderItem) {
        val file = File(context.cacheDir.absolutePath + FILE_NAME)

        if( file.exists() ) {
            val orderJson = Gson().fromJson(file.readText(), Order::class.java) as Order
            val orders = addToBasketList( orderJson, orderItem)
            file.writeText(Gson().toJson( orders))
        } else {
            file.writeText(Gson().toJson(Order(mutableListOf(orderItem))))
        }
    }

    private fun saveBasketOrder( order: Order) {
        val file = File(context.cacheDir.absolutePath + FILE_NAME)
        file.writeText( Gson().toJson( order))
    }

    override fun deleteBasketFile() {
        val file = File(context.cacheDir.absolutePath + FILE_NAME)
        file.delete()
    }

    override fun deleteItemBasket(index: Int) {
        loadBasketOrder().let{ orders ->
            orders.items.removeAt( index)
            saveBasketOrder( orders)
        }
    }

    private fun addToBasketList(order: Order, orderItem: OrderItem) : Order {
        order.items.firstOrNull { it.item ==  orderItem.item}?.let {
            it.quantity += orderItem.quantity
        } ?: run {
            order.items.add(orderItem)
        }
        return order
    }

    companion object {
        const val FILE_NAME = "/basket.json"
        const val TAG = "BasketService"
    }
}