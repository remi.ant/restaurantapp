package fr.isen.antunes.restaurantapp.enumeration

enum class CategoryEnum(val text: String, val id: Int) {
    ENTREE("Entrées", 0),
    PLAT("Plats", 1),
    DESERT("Déserts", 2);
}