package fr.isen.antunes.restaurantapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CommandResponse(
        @SerializedName("code")val code: Int
) : Serializable
