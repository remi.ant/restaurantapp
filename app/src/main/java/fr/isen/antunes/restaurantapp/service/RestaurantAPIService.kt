package fr.isen.antunes.restaurantapp.service

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import fr.isen.antunes.restaurantapp.enumeration.CategoryEnum
import fr.isen.antunes.restaurantapp.model.*
import fr.isen.antunes.restaurantapp.utils.QUERY_DISH_PREFERENCES
import org.json.JSONObject

interface RestaurantAPIInterface {

    fun getDishesPostData(): JSONObject
    fun getDishes( category: CategoryEnum, postData: JSONObject): MutableLiveData<List<Item>>

    fun getRegisterPostData( register: Register): JSONObject
    fun getRegister( postData: JSONObject): MutableLiveData<RegisterResponse>

    fun getLoginPostData( register: Register): JSONObject
    fun getLogin( postData: JSONObject): MutableLiveData<RegisterResponse>

    fun getOrderPostData( userId: Int, orders: Order): JSONObject
    fun getOrder( postData: JSONObject): MutableLiveData<CommandResponse>
}


/**
 * Live Data
 * https://www.raywenderlich.com/10391019-livedata-tutorial-for-android-deep-dive#toc-anchor-002
 */
class RestaurantAPIService(
    val context: Context,
    private val preferencesService: UserPreferencesService
): RestaurantAPIInterface {

    override fun getDishesPostData(): JSONObject {
        return JSONObject().put("id_shop", "1")
    }

    override fun getDishes( category: CategoryEnum, postData: JSONObject): MutableLiveData<List<Item>> {
        var categoryData = MutableLiveData<List<Item>>()

        preferencesService.readPreferences( QUERY_DISH_PREFERENCES)?.let {
            val parsedData = GsonBuilder().create().fromJson( it, DataJSON::class.java)
            categoryData.postValue(parsedData.data[category.id].items)
        } ?:run {
            apiQuery(API_URL_MENU, postData).observeForever {
                val parsedData: DataJSON = Gson().fromJson(it, DataJSON::class.java)
                categoryData.postValue(parsedData.data[category.id].items)
                preferencesService.writePreferences( QUERY_DISH_PREFERENCES, it.toString())
            }
        }
        return categoryData
    }

    override fun getRegisterPostData( register: Register): JSONObject {
        val jsonObject = JSONObject().put("id_shop", "1")
        return register.toRegisterData( jsonObject)
    }

    override fun getRegister( postData: JSONObject): MutableLiveData<RegisterResponse> {
        val categoryData = MutableLiveData<RegisterResponse>()
        apiQuery( API_URL_REGISTER, postData).observeForever {
            val parsedData: RegisterResponse = Gson().fromJson(it, RegisterResponse::class.java)
            categoryData.postValue( parsedData)
        }
        return categoryData
    }

    override fun getLoginPostData( register: Register): JSONObject {
        val jsonObject = JSONObject().put("id_shop", "1")
        return register.toLoginData( jsonObject)
    }

    override fun getLogin( postData: JSONObject): MutableLiveData<RegisterResponse> {
        val categoryData = MutableLiveData<RegisterResponse>()
        apiQuery( API_URL_LOGIN, postData).observeForever {
            val parsedData: DataRegisterResponse = Gson().fromJson(it, DataRegisterResponse::class.java)
            categoryData.postValue( parsedData.data)
        }
        return categoryData
    }

    override fun getOrderPostData( userId: Int, orders: Order): JSONObject {
        val jsonObject = JSONObject().put("id_shop", "1")
        jsonObject.put( "id_user", userId.toString())
        return jsonObject.put( "msg", orders)
    }

    override fun getOrder( postData: JSONObject): MutableLiveData<CommandResponse> {
        val categoryData = MutableLiveData<CommandResponse>()
        apiQuery( API_URL_ORDER, postData).observeForever {
            Log.i("ServiceAPi", "get order")
            val parsedData: CommandResponse = Gson().fromJson(it, CommandResponse::class.java)
            categoryData.postValue( parsedData)
            Log.i("ServiceAPi", "get order -> responses")
        }
        return categoryData
    }

    private fun apiQuery( url: String, postData: JSONObject): LiveData<JsonElement> {
        val result : MutableLiveData<JsonElement> = MutableLiveData<JsonElement>()
        val request = JsonObjectRequest( Request.Method.POST, url, postData, {
            response ->
                Log.i( TAG, "api query successful-> $response")
                val element: JsonElement = Gson().fromJson( response.toString(), JsonElement::class.java)
                result.postValue( element)
        }, {
            error ->
                Log.i( TAG, "api query error-> $error")
        })
        Volley.newRequestQueue(context).add( request)
        return result
    }

    companion object {
        const val TAG = "RestaurantApiService"
        const val API_URL_MENU = "http://test.api.catering.bluecodegames.com/menu"
        const val API_URL_REGISTER = "http://test.api.catering.bluecodegames.com/user/register"
        const val API_URL_LOGIN = "http://test.api.catering.bluecodegames.com/user/login"
        const val API_URL_ORDER = "http://test.api.catering.bluecodegames.com/user/order"
    }
}
