package fr.isen.antunes.restaurantapp.adapter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import fr.isen.antunes.restaurantapp.fragment.DetailFragment

class DetailFragmentAdapter(
    activity: AppCompatActivity,
    private val item: List<String>
): FragmentStateAdapter( activity) {

    override fun getItemCount(): Int = item.size

    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        val fragment = DetailFragment()
        fragment.arguments = Bundle().apply {
            putString( DetailFragment.ARG_OBJECT, item[position])
        }
        return fragment
    }
}