package fr.isen.antunes.restaurantapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.isen.antunes.restaurantapp.R
import fr.isen.antunes.restaurantapp.databinding.CartItemListBinding
import fr.isen.antunes.restaurantapp.model.OrderItem

class CartAdapter(
        private val orders: MutableList<OrderItem>,
        private val itemClickListener: (Int) -> Unit
) : RecyclerView.Adapter<CartAdapter.CartHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartHolder {
        val itemBinding = CartItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return CartHolder(itemBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CartHolder, position: Int) {
        val oi: OrderItem = orders[position]

        holder.cardTitle.text = oi.item.title
        holder.cardPrice.text = "${oi.quantity * oi.item.prices[0].price.toDouble()}€"
        holder.cardQuantity.text = "qty: ${oi.quantity}"

        if (oi.item.images.first().isNotEmpty()) {
            Picasso.get()
                    .load(orders[position].item.images[0])
                    .into(holder.cardImage)
        } else {
            holder.cardImage.setImageResource( R.drawable.ic_bocage)
        }

        holder.cardDeleteButton.setOnClickListener {
            orders.removeAt( position)
            notifyDataSetChanged()
            itemClickListener.invoke( position)
        }

    }

    override fun getItemCount(): Int = orders.size

    class CartHolder(binding: CartItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        val cardTitle: TextView = binding.orderCardTitle
        val cardPrice: TextView = binding.orderCardPrice
        val cardImage: ImageView = binding.orderCardImage
        val cardQuantity: TextView = binding.orderCardQuantity
        val cardDeleteButton: Button = binding.orderCardDelete
    }

    companion object {
        const val TAG = "CartAdapter"
    }
}