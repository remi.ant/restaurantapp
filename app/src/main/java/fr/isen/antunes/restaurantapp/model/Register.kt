package fr.isen.antunes.restaurantapp.model

import org.json.JSONObject
import java.io.Serializable

data class Register(
        val lastName: String,
        val firstName: String,
        val address: String,
        val email: String,
        val password: String
) : Serializable {

    fun toRegisterData( params: JSONObject): JSONObject {
        params.put("firstname", firstName)
        params.put("lastname", lastName)
        params.put("adresse", address)
        params.put("email", email)
        params.put("password", password)
        return params
    }

    fun toLoginData( params: JSONObject): JSONObject {
        params.put( "email", email)
        params.put("password", password)
        return params
    }
}