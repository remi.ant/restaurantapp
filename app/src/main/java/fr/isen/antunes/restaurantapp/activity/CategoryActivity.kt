package fr.isen.antunes.restaurantapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import fr.isen.antunes.restaurantapp.adapter.CategoryAdaptor
import fr.isen.antunes.restaurantapp.databinding.ActivityCategoryBinding
import fr.isen.antunes.restaurantapp.enumeration.CategoryEnum
import fr.isen.antunes.restaurantapp.model.Item
import fr.isen.antunes.restaurantapp.service.RestaurantAPIService
import fr.isen.antunes.restaurantapp.service.UserPreferencesService
import fr.isen.antunes.restaurantapp.utils.CATEGORY
import fr.isen.antunes.restaurantapp.utils.ITEM
import fr.isen.antunes.restaurantapp.utils.QUERY_DISH_PREFERENCES
import org.koin.android.ext.android.inject

class CategoryActivity : BaseActivity() {

    private lateinit var binding: ActivityCategoryBinding
    private val apiService by inject<RestaurantAPIService>()
    private val preferencesService by inject<UserPreferencesService>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupView()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(HomeActivity.TAG,"destroy")
    }

    private fun setupView() {
        val type = intent.getSerializableExtra(CATEGORY) as CategoryEnum

        binding.categoryTitle.text = type.text

        binding.refreshSwip.setOnRefreshListener {
            preferencesService.removeOnePreferences( QUERY_DISH_PREFERENCES)
            loadDishes(type)
            binding.refreshSwip.isRefreshing = false
        }
        Log.i( TAG, "load dishes")
        loadDishes(type)
    }

    private fun loadDishes( type : CategoryEnum) {
        apiService.getDishes( type, apiService.getDishesPostData()).observe( this, {
            it?.let {
                Log.i( TAG, "api resutl -> $it")
                displayCategories( it)
            } ?: run {
                Log.i( TAG, "no api result")
            }
        })
    }

    private fun displayCategories( categories: List<Item>) {
        binding.categoryList.layoutManager = LinearLayoutManager(this)
        binding.categoryList.adapter = CategoryAdaptor(categories) {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(ITEM, it)
            startActivity(intent)
        }
    }

    companion object {
        const val TAG = "CategoryActivity"
    }
}